/// <reference types="cypress" />

import BasePage from './basePage'

export const EXPORTPATH = 'cypress/fixtures/companyDetails.json'
export const HEADER = 'h1:nth-child(1)'
export const COMPANYLIST = 'a.key'
export const LOGO = 'div.companyLogoWrapper'
export const LOGOFILEPATH = 'cypress/fixtures/companyLogos'
export const DETAILSBOXES = 'div.gfdCompanyDetailsCol'
export const DETAILTITLES = 'div.gfdCompanyDetailsCol > div.gfdCompanyDetailsTitle'

export default class CompanyPage extends BasePage {
    createFile() {
        // create or clear companyDetails.json add [0] index for readability
        cy.writeFile(EXPORTPATH, '[ "Company Details"')
    }

    closeFile() {
        // close the array in companyDetails.json so file is valid JSON
        cy.writeFile(EXPORTPATH, ' ]', {flag: 'a+'})
    }

    getCompanyList() {
        // returns a list of companies on page
        return cy.get(COMPANYLIST)
    }

    getFirstCompany() {
        // get the first company in the list
        cy.get(COMPANYLIST).first().should('exist').click()
    }

    getLastCompany() {
        // get the last company in the list
        cy.get(COMPANYLIST).last().should('exist').click()
    }

    getNthCompany(n) {
        // if there are more than 3 companies in the list get the 3rd company
        cy.get(COMPANYLIST, { log: false }).its('length').then((length) => {
            if (length > n) {
                cy.get(COMPANYLIST, { log: false }).each((company, index) => {
                    if (index === n - 1) {
                        cy.wrap(company).should('exist').click()
                    }
                })
            }
        })
    }

    exportDetails(json) {
        // append new index to array on new line for readability
        cy.writeFile(EXPORTPATH, ',\n', {flag: 'a+'})

        // write company details to index
        cy.writeFile(EXPORTPATH, json, {flag: 'a+'})
    }

    captureDetails() {
        // create JSON to house company details
        var companyDetails = {}

        // get the company name from header text
        cy.get(HEADER).invoke('text').then((text) => {
            // add the name of the company to companyDetails JSON
            companyDetails["Company Name"] = text
        })

        // get the logo wrapper and check that child has src attr
        cy.get(LOGO).children().should('have.attr', 'src').then((src) => {
            // get the company name and replace invalid characters to create good file name
            var logoName = companyDetails["Company Name"].replaceAll(/[\.\/]/g, '').replaceAll('  ', ' ').replaceAll(' ', '_') + '.jpeg'
            // download the logo
            cy.downloadFile('https://www.medicines.org.uk/' + src, LOGOFILEPATH, logoName)
            // add the file path of the logo to companyDetails JSON
            companyDetails["Logo Path"] = LOGOFILEPATH + '/' + logoName
            cy.readFile(companyDetails["Logo Path"]).should('exist')
        })
        
        // get the array of titles containing company details
        cy.get(DETAILTITLES).should('have.class', 'gfdCompanyDetailsTitle').each((title, index) =>{

            // get the text of the titles
            cy.get(title).invoke('text').then((title) => {

                // get the text following the titles
                cy.get(DETAILSBOXES).find('p').eq(index).invoke('text').then((text) => {

                    // add details to companyDetails JSON with key "title": "text"
                    // remove all \n & \t characters in text
                    companyDetails[title] = text.replaceAll(/[\n\t]/g, '')
                })
            })

        })
        
        // export the company details to companyDetails.json
        this.exportDetails(companyDetails)

        // return to company list
        cy.go('back', { timeout: 10000 })
    }
}