/// <reference types="cypress" />

export default class BasePage {
    verifyText(identifier, commonString) {
        // verifies text matches commonString
        cy.get(identifier)
            .should('have.text', commonString)
    }

    verifyTextBlock(identifier, commonString) {
        // verifies a paragraph contains commonString
        cy.get(identifier)
            .should('include.text', commonString)
    }

    visitPageUrl(pageUrl) {
        // vist url provided
        cy.visit(pageUrl)
    }

    visitLink(href) {
        // given a link send a request to check status code
        cy.request({
            url: BASEURL + href,
            failOnStatusCode: false,
        })
            // if response is ok visit link
            .then((response) => {
                if (response.isOkStatusCode) {
                    cy.visit({
                        url: BASEURL + href,
                        sendImmediately: false
                    })
                }
            })
    }
}