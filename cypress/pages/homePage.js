/// <reference types="cypress" />

import { homePageStrings } from '../fixtures/commonStrings'
import BasePage from './basePage'

export const PAGEURL = "https://www.medicines.org.uk/emc/browse-companies"
export const HEADER = 'div.col-md-12.title > h1:nth-child(1)'
export const SUBHEADER = 'div.col-md-12.browse-head > h2:nth-child(1)'
export const PAGES = 'div.col-md-12.browse-head > ul.browse:nth-child(2)'

export default class HomePage extends BasePage {
    getHeader() {
        // returns page header
        return cy.get(HEADER)
    }

    getSubHeader() {
        // returns page subheader
        return cy.get(SUBHEADER)
    }

    getPages() {
        // returns list of A-Z pages
        return cy.get(PAGES)
    }

    verifyHeader() {
        // verifies page header matches expected string
        this.verifyText(HEADER, homePageStrings.HEADER)
    }

    verifySubHeader() {
        // verifies page subheader matches expected string
        this.verifyText(SUBHEADER, homePageStrings.SUBHEADER)
    }

    visitPage() {
        // visit home page url
        return this.visitPageUrl(PAGEURL)
    }
} 