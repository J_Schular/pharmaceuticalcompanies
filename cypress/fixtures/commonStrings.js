let homePageStrings = {
    "HEADER": "Browse pharmaceutical company names",
    "SUBHEADER": "Companies beginning with A"
}

module.exports = {
    homePageStrings,
}