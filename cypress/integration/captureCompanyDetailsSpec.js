/// <reference types="cypress" />

import HomePage from "../pages/homePage"
import CompanyPage from "../pages/companyPage"

const homePage = new HomePage()
const companyPage = new CompanyPage()

describe('A-Z Pharmaceutical Companies', () => {
    before(() => {
        // visit homepage
        homePage.visitPage()

        // click through popup accepting cookies
        cy.get('#onetrust-accept-btn-handler').click()

        // verify on the correct page
        homePage.verifyHeader()
        homePage.verifySubHeader()

        // create/clear JSON file
        companyPage.createFile()
    })

    it('Capture pharmaceutical company details', () => {
        // find the pages with active links
        homePage.getPages().children().find('a').each((page) => {
            // for each page find its href
            cy.wrap(page).invoke('attr', 'href').then((href) => {
                // visit the link
                basePage.visitLink(href)

                // get the list of companies for current page
                companyPage.getCompanyList().its('length').then((length) => {
                    // if there is at least one company we can click the first company
                    if (length > 0) {
                        // navigate through to first company page
                        companyPage.getFirstCompany()

                        // capture company details
                        companyPage.captureDetails()
                    }

                    // if there are more than 3 companies we can click the first, third and last company
                    if (length > 3) {
                        // navigate through to third company page
                        companyPage.getNthCompany(3)

                        // capture company details
                        companyPage.captureDetails()
                    }

                    // if there are at least 2 companies we can click the first and last company
                    if (length > 1) {
                        // navigate through to last company page
                        companyPage.getLastCompany()

                        // capture company details
                        companyPage.captureDetails()
                    }                   
                })
            })
        })
        // append ending of array in file to create valid JSON file
        companyPage.closeFile()
    })
})